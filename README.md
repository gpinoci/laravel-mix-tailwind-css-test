### Watch task running times:

##### CentOS 6, Node 10.20.1
- Basic: ~10s;
- With custom tailwind config: ~14s;
- With sourcemaps: ~20s;
- With both: ~24s;

##### Windows 10, Node 12.13.1
- Basic: ~6s;
- With custom tailwind config: ~7.5s;
- With sourcemaps: ~9.5s;
- With both: ~12s;
