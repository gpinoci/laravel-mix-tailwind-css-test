module.exports = {
    purge: false,
    theme: {
        screens: {
            sm: '640px',
            md: '768px',
            lg: '1024px',
            xl: '1280px',
            xxl: '1600px',
        },
        container: {
            center: true,
            padding: {
                default: '1rem',
                md: '3rem',
                xxl: '13rem'
            }
        },
        extend: {
            fontSize: {
                '7xl': '5rem',
                '8xl': '6rem',
            },
            colors: {
                primary: '#EE3124',
            },
            spacing: {
                '7': '1.75rem',
                '9': '2.25rem',
                '11': '2.75rem',
                '13': '3.25rem',
                '14': '3.5rem',
                '15': '3.75rem',
                '17': '4.25rem',
                '18': '4.5rem',
                '19': '4.75rem',
                '36': '9rem',
                '96': '24rem',
                '128': '32rem',
            },
            opacity: {
                '05': '.05',
                '10': '.1',
                '20': '.2',
                '80': '.8',
                '90': '.9',
                '95': '.95',
            },
            zIndex: {
                '-1': '-1',
            },
            transitionTimingFunction: {
                'default-easing': 'cubic-bezier(.77, 0, .18, 1)',
                'default-easing-reverse': 'cubic-bezier(.82, 0, .23, 1)',
            },
        }
    },
    variants: {},
    plugins: [],
}